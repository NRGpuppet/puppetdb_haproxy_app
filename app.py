#coding:utf-8

# Rudimentary app for puppetDB -> HAproxy updates
#
# Refer to README.md for more info.

import os
import sys
import subprocess
import logging
import time
import flask
import util
import threading

from apscheduler.schedulers.background import BackgroundScheduler

from pypuppetdb import connect
from pypuppetdb.QueryBuilder import *

from pyhaproxy.parse import Parser
from pyhaproxy.render import Render
import pyhaproxy.config as config

app = flask.Flask(__name__)

# Config from environment vars
app.config.update(
    polling_interval=os.environ.get('POLLING_INTERVAL', '5'),
    puppetdb_host=os.environ.get('PUPPETDB_HOST'),
    puppetdb_port=os.environ.get('PUPPETDB_PORT'),
    puppet_env=os.environ.get('PUPPET_ENVIRONMENT'),
    ssl_key=os.environ.get('SSL_KEY'),
    ssl_cert=os.environ.get('SSL_CERT'),
    haproxy_cfg_file=os.environ.get('HAPROXY_CFG_FILE'),
    haproxy_reload_cmd=os.environ.get('HAPROXY_RELOAD_CMD'),
)

# Logging configuration
stderr_log_handler = logging.StreamHandler()
app.logger.addHandler(stderr_log_handler)
app.logger.setLevel(logging.INFO)

def get_logger_timestamp():
    return "[" + time.strftime("%Y-%m-%d %H:%M:%S +0000", time.gmtime()) + "]"

# Query puppetDB and update HAproxy
def refresh_puppetdb_haproxy():
    config_changed = False

    # puppetDB connection
    db = connect(
        host=app.config['puppetdb_host'],
        port=app.config['puppetdb_port'],
        ssl_key=app.config['ssl_key'],
        ssl_cert=app.config['ssl_cert'],
        ssl_verify=False, # puppet CA is self-signed
    )

    # Build puppetDB query
    res_query = AndOperator()
    res_query.add(EqualsOperator('type', 'Class'))
    res_query.add(EqualsOperator('title', 'Profile::Haproxy'))

    in_op = InOperator('certname')
    fr_op = FromOperator('nodes')
    ex_op = ExtractOperator()
    ex_op.add_field('certname')
    ex_op.add_query(NullOperator("deactivated", True))
    fr_op.add_query(ex_op)
    in_op.add_query(fr_op)

    # Run query on specified endpoint, e.g. resources
    resources = db.resources(query=res_query)
    for res in resources:
        print(res.parameters)

    if config_changed:
        # Reload haproxy
        try:
            subprocess.check_call(haproxy_reload_cmd, shell=True)
        except subprocess.CalledProcessError:
            app.logger.exception("%s Error reloading haproxy", get_logger_timestamp())


# Handle requests for "/refresh"
@app.route("/refresh", methods=("GET",))
def refresh_get_handler():
    app.logger.info("%s Received refresh request", get_logger_timestamp())
    refresh_puppetdb_haproxy()
    return flask.Response(status=200)

@app.before_request
def log_request():
    app.logger.debug("%s Received request: %s %s", get_logger_timestamp(), flask.request.method,
                     flask.request.url)

# Reload haproxy on start
refresh_puppetdb_haproxy()

# Schedule polling in a backgrounded thread, so that app can respond to requests from '/refresh' in parallel
sched = BackgroundScheduler(daemon=True)
sched.add_job(refresh_puppetdb_haproxy,'interval',seconds=int(app.config['polling_interval']))
sched.start()

if __name__ == '__main__':
    app.run(debug=True)
