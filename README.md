PuppetDB HAproxy admin app
==========================

Python flask app for puppetDB REST / HAproxy API updates

Install as Service on CentOS 7
==============================

TODO: Fix gunicorn syslog, not working.

Create dedicated user and copy Python app there:
```
$ sudo useradd -s /usr/sbin/nologin gunicorn
$ sudo git clone https://bitbucket.org/NRGpuppet/puppetdb_haproxy_app /home/gunicorn/puppetdb_haproxy_app
$ sudo chown -R gunicorn.gunicorn /home/gunicorn/puppetdb_haproxy_app
```

Next, create a dedicated cert/key pair for this service on the PuppetDB host and copy to /home/gunicorn/ssl as referenced below.
```
$ sudo puppet cert generate puppetdb-haproxy-hostname
```

Likewise, you'll also need to add the puppet-ca CA cert to your system's ca-certificates for ssl_verify to work.  That CA cert can be found at $ssldir/certs/ca.pem on puppet-managed nodes.
https://pypuppetdb.readthedocs.io/en/latest/quickstart.html#configure-pypuppetdb-for-ssl

Create startup environment /etc/sysconfig/puppetdb_haproxy_app with config params.  All those listed below are required.
```
POLLING_INTERVAL=5
PUPPETDB_HOST=puppetdb.domain.com
PUPPETDB_PORT=8081
PUPPET_ENVIRONMENT=production # Not implemented yet
SSL_KEY=/home/gunicorn/ssl/puppetdb-haproxy-hostname.key
SSL_CERT=/home/gunicorn/ssl/puppetdb-haproxy-hostname.crt
HAPROXY_SOCKET_DIR=/var/lib/haproxy/stats
HAPROXY_CFG_FILE=/etc/haproxy/haproxy_puppetdb.cfg
HAPROXY_RELOAD_CMD="/usr/bin/systemctl reload haproxy"
```

Create systemd service definition /etc/systemd/system/puppetdb_haproxy_app.service:
```
[Unit]
Description=Gunicorn instance for PuppetDB HAproxy admin app
After=network.target

[Service]
PIDFile=/home/gunicorn/gunicorn.pid
Type=forking
User=gunicorn
Group=gunicorn
WorkingDirectory=/home/gunicorn/puppetdb_haproxy_app
EnvironmentFile=/etc/sysconfig/puppetdb_haproxy_app
ExecStart=/usr/bin/gunicorn -D -p /home/gunicorn/gunicorn.pid --log-file /var/log/gunicorn/puppetdb_haproxy_app.log --capture-output --bind 0.0.0.0:5000 app:app
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s QUIT $MAINPID

[Install]
WantedBy=multi-user.target
```

Open firewall port 5000 and give guincorn user limited sudo access.
```
$ sudo firewall-cmd --permanent --zone=public --add-port=5000/tcp
$ sudo firewall-cmd --reload
$ echo 'gunicorn ALL=(ALL) NOPASSWD: /opt/some/command' | sudo tee -a /etc/sudoers
```

Create log directory.
```
$ sudo mkdir /var/log/gunicorn/
$ sudo chown gunicorn.gunicorn /var/log/gunicorn/
```

Finally, enable and start the new puppetdb_haproxy_app service.
```
$ sudo systemctl start puppetdb_haproxy_app
$ sudo systemctl enable puppetdb_haproxy_app
```

Reference:
https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-centos-7

Usage
=====

To deploy the app, you can:

   + Add it to an instance you own, install its dependencies
   + Use a PaaS, either deployed on your own infrastructure (CloudFoundry), or on public infrastructure (e.g. Heroku)


Installing Dependencies
-----------------------

To install the app's dependencies, do the following:

  + Ensure that you have the [Python Pip installer][0] available on your system
  + Run `pip install -r requirements.txt`, from the root of the project


Configuring the app
-------------------

The application is configured by passing its configuration through environment variables (i.e. it's a [12-factor app][10]).

If you are using Honcho or Foreman (as suggested below), you can simply input those environment variables in the `.env`
file.

The following environment variables are used:

  + `VARIABLE`: Dummy variable

Running the app
---------------

Once you have installed and configured the app, you can launch it by running `honcho start web`. The app will listen on
port `5000` by default.

If you're launching the app on an instance, you'll probably want to daemonize it. To do so, navigate to the app
directory, and then run:

    gunicorn --daemon --bind 0.0.0.0:5000 app:app

Bear in mind that `gunicorn` will not read your `.env` file, so you have to pass needed environment variables through other means.

One option is:

    VARIABLE=xxxx gunicorn --daemon --bind 0.0.0.0:5000 app:app

To run gunicorn on the console (i.e. for test/debugging), stop CentOS 7 service created above, cd to a cloned repo of this repo (e.g. /home/gunicorn/puppetdb_haproxy_app) and launch gunicorn manually:

    VARIABLE=xxxx /usr/bin/gunicorn --log-level debug --bind 0.0.0.0:5000 app:app

For further information, you should check the [Gunicorn documentation][3].


Issues
======

Please report issues and ask questions on the project's Git repository, in the [issues section][2].


License
=======

View `LICENSE`.

  [0]: http://www.pip-installer.org/
  [10]: http://12factor.net/
  [2]: https://bitbucket.org/NRGpuppet/puppetdb_haproxy_app/issues
  [3]: http://gunicorn-docs.readthedocs.org/en/latest/configure.html
